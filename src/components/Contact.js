import { withRouter } from "react-router-dom";
import React from "react";
import { inject, observer } from "mobx-react";
import { Grid, Segment, Header, Form, Button } from "semantic-ui-react";

@inject("contactStore")
@withRouter
@observer
export default class Contact extends React.Component {
  updateInput = e =>
    this.props.contactStore.updateInput(e.target.value, e.target.id);
  validateInput = e =>
    this.props.contactStore.validateInput(e.target.value, e.target.id);
  submitForm = (e) =>{
      e.preventDefault();
    this.props.contactStore.submitForm()
    //.then((res) => console.log(res));
  }

  render() {
    const { values, errorName, errorEmail, errorReason } = this.props.contactStore;
    return (
      <div className="login-form">
        <style>{`
      body > div,
      body > div > div,
      body > div > div > div.login-form {
        height: 100%;
      }
    `}</style>
        <Grid
          textAlign="center"
          style={{ height: "100%" }}
          verticalAlign="middle"
        >
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as="h2" color="teal" textAlign="center">
              Contact
            </Header>
            <Form size="large" onSubmit={this.submitForm}>
              <Segment stacked>
                <Form.Input
                  fluid
                  icon="user"
                  iconPosition="left"
                  placeholder="Name"
                  id="name"
                  value={values.name}
                  onChange={this.updateInput}
                  onBlur={this.validateInput}
                />
                <span hidden={!errorName} style={{color:"red"}}>Nombre muy corto</span>
                <Form.Input
                  fluid
                  icon="mail"
                  iconPosition="left"
                  placeholder="Email"
                  id="email"
                  value={values.email}
                  onChange={this.updateInput}
                  onBlur={this.validateInput}
                />
                <span hidden={!errorEmail} style={{color:"red"}}>Email no válido</span>
                <Form.TextArea
                  fluid
                  placeholder="Reason..."
                  id="reason"
                  value={values.reason}
                  onChange={this.updateInput}
                  onBlur={this.validateInput}
                />
                <span hidden={!errorReason} style={{color:"red"}}>Mensaje muy corto</span>

                <Button color="teal" fluid size="large">
                  Send Email
                </Button>
              </Segment>
            </Form>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}
