import { observable, action } from 'mobx';
import agent from '../agent';
import toastr from 'toastr';
const TOASTR_OPTIONS = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
toastr.options = TOASTR_OPTIONS;

class ContactStore {

     @observable values = {
         name : '',
         email : '',
         reason : ''
     };
     @observable errorName = false;
     @observable errorEmail = false;
     @observable errorReason = false;

    @action updateInput(value, field) {
        this.values[field] = value;
    }

    @action submitForm(){
        if(this.errorName || this.errorEmail || this.errorReason || !this.values.name || !this.values.email || !this.values.reason){
            toastr['error']('Error! Please check again form!')
        }else{
            toastr['success']('Success! Email sent!')
            return agent.Contact.sendEmail({data : {email: this.values.email, subject: this.values.name, comment: this.values.reason}});
        }
    }

    @action validateInput(value, field) {
        if(field === 'name'){
            if(value.length < 4){
                this.errorName = true;
            }else{
                this.errorName = false;
            }
        } else if (field === 'email'){
            if (!value.match(/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/)){
                this.errorEmail = true;
            }else{
                this.errorEmail = false;
            }
        }else if(field === 'reason'){
            if(value.length < 20){
                this.errorReason = true;
            }else{
                this.errorReason = false;
            }
        }
    }

}

export default new ContactStore();