> ### React + Mobx, small project to test the MobX technology in React, with a contact form, connecting to the MoleculerJS backend

## Getting started

To get the frontend running locally:

- Clone this repo
- `npm install` to install all req'd dependencies
- `npm start` to start the local server (this project uses create-react-app)


### Making requests to the backend API


If you want to change the API URL to a local server, simply edit `src/agent.js` and change `API_ROOT` to the local server's URL (i.e. `localhost:3000/api`)


## Functionality overview


**General functionality:**

- Contact

**The general page breakdown looks like this:**

- Contact (URL /#/Contact)
    - Form
    - Validation
    - Toastr
    - Send Email
